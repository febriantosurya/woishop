const express = require('express');
const db = require('./config/database');
const dotenv = require('dotenv');
const router = require('./routes/index');
dotenv.config();

const app = express();
const PORT = 3000;

try {
  db.authenticate().then(() => {
    console.log('Database Connected.');
  })
} catch (error) {
  console.log(error);
}

app.use(express.json());
app.use(router);

app.listen(PORT, () => {
  console.log(`Running on PORT : ${PORT}`);
});