const { Sequelize } = require('sequelize');
const db = require('../config/database');
const { Product } = require('./product');
const { Users } = require('./user');

const Carts = db.define('carts', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  quantity: Sequelize.BIGINT
}, {
  freezeTableName: true,
  timestamps: false,
  createdAt: false,
  updatedAt: false
});

Product.hasMany(Carts);
Carts.belongsTo(Product);

Users.hasMany(Carts);
Carts.belongsTo(Users);

module.exports = {
  Carts
};