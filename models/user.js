const { Sequelize } = require('sequelize');
const db = require('../config/database');

const Users = db.define('credentials', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true
  },
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  refresh_token: {
    type: Sequelize.TEXT
  }
}, {
  freezeTableName: true,
  timestamps: false,
  createdAt: false,
  updatedAt: false
});

module.exports = {
  Users
};