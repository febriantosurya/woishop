const { Sequelize } = require('sequelize');
const db = require('../config/database');

const Category = db.define('categories', {
  id : {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  name : Sequelize.TEXT
}, {
  freezeTableName: true,
  timestamps: false,
  createdAt: false,
  updatedAt: false,
});

const Product = db.define('products', {
  id : {
    type: Sequelize.BIGINT,
    primaryKey: true
  },
  categoryId : Sequelize.BIGINT,
  name : Sequelize.TEXT,
  stock : Sequelize.BIGINT
}, {
  timestamps: false,
  createdAt: false,
  updatedAt: false,
});

Category.hasMany(Product);
Product.belongsTo(Category);

module.exports = {
  Category, Product
}