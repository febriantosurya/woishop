const jwt = require('jsonwebtoken');

const verif = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return res.status(401).send('Token is required!');
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    if (err) return res.status(403).send('Invalid token!');
    req.email = decoded.email;
    next();
  });
};

exports.verif = verif;