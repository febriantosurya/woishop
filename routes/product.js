const express = require('express');
const product = require('../controllers/product');
const { verif } = require('../middleware/verifToken');

const router = express.Router();

router.get('/category', verif, product.getCategory);
router.get('/all', verif, product.getAllProduct);
router.get('/category/:id', verif, product.getProductByCategory);
router.get('/detail/:id', verif, product.getDetailProduct);

module.exports = router;