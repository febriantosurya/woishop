const express = require('express');
const getUser = require('./user');
const getProduct = require('./product');
const getCart = require('./cart');

const router = express.Router();

router.use('/account', getUser);
router.use('/product', getProduct);
router.use('/cart', getCart);

module.exports = router;