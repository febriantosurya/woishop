const express = require('express');
const cart = require('../controllers/cart');
const { verif } = require('../middleware/verifToken');

const router = express.Router();

router.post('/add', verif, cart.add);
router.post('/update', verif, cart.update);
router.post('/remove', verif, cart.remove);
router.get('/show', verif, cart.showAll);
router.get('/:id', verif, cart.showProduct);

module.exports = router;
